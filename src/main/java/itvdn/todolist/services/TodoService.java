package itvdn.todolist.services;

import itvdn.todolist.Exceptions.CustomEmptyDataException;
import itvdn.todolist.domain.PlainObjects.TodoPojo;
import itvdn.todolist.domain.Tag;
import itvdn.todolist.domain.Todo;
import itvdn.todolist.domain.User;
import itvdn.todolist.repositories.TodoRepository;
import itvdn.todolist.repositories.UserRepository;
import itvdn.todolist.services.interfaces.ITagService;
import itvdn.todolist.services.interfaces.ITodoService;
import itvdn.todolist.utils.Convertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class TodoService implements ITodoService {

    private final Convertor convertor;
    private final ITagService tagService;
    private final TodoRepository todoRepository;
    private final UserRepository userRepository;

    @Autowired
    public TodoService(Convertor convertor,
                       ITagService tagService,
                       TodoRepository todoRepository,
                       UserRepository userRepository) {
        this.convertor = convertor;
        this.tagService = tagService;
        this.todoRepository = todoRepository;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public TodoPojo createTodo(Todo todo, Long userId) {
        Optional<User> todoUserOptional = userRepository.findById(userId);

        if (todoUserOptional.isPresent()) {
            Set<Tag> tags = new HashSet<>();
            tags.addAll(todo.getTagList());

            todo.getTagList().clear();

            todo.setUser(todoUserOptional.get());
            todoRepository.save(todo);

            tags.stream().map(tagService::findOrCreate).collect(Collectors.toSet()).forEach(todo::addTag);

            return convertor.todoToPojo(todo);
        } else {
            throw new CustomEmptyDataException("unable to get user for todo");
        }
    }

    @Override
    @Transactional
    public TodoPojo getTodo(Long id) {
        Optional<Todo> todoOptional = todoRepository.findById(id);
        if (todoOptional.isPresent()) {
            return convertor.todoToPojo(todoOptional.get());
        }
        throw new CustomEmptyDataException("unable to get todo");
    }

    @Override
    @Transactional
    public TodoPojo updateTodo(Todo source, Long todoId) {
        Optional<Todo> targetOptional = todoRepository.findById(todoId);
        if (targetOptional.isPresent()) {
            Todo target = targetOptional.get();

            target.setName(source.getName());
            target.setComment(source.getComment());
            target.setStartDate(source.getStartDate());
            target.setEndDate(source.getEndDate());
            target.setImportant(source.isImportant());
            target.setPriority(source.getPriority());

            todoRepository.save(target);

            return convertor.todoToPojo(target);
        } else {
            throw new CustomEmptyDataException("unable to update todo");
        }
    }

    @Override
    @Transactional
    public String  deleteTodo(Long id) {
        Optional<Todo> todoForDeleteOptional = todoRepository.findById(id);
        if (todoForDeleteOptional.isPresent()) {
            Todo todoForDelete = todoForDeleteOptional.get();
            todoForDelete.getTagList().stream().collect(Collectors.toList()).forEach(tag -> tag.removeTodo(todoForDelete));
            todoRepository.delete(todoForDeleteOptional.get());
            return "Todo with id:" + id + " was successfully deleted";
        } else {
            throw new CustomEmptyDataException("unable to delete todo");
        }
    }

    @Override
    @Transactional
    public List<TodoPojo> getAllTodos(Long userId) {
         Optional<User> userOptional = userRepository.findById(userId);
         if (userOptional.isPresent()) {
             return todoRepository.findAllByUser(userOptional.get()).stream().map(convertor::todoToPojo).collect(Collectors.toList());
         }
         return new ArrayList<>();
    }
}
