package itvdn.todolist.services;

import itvdn.todolist.Exceptions.CustomEmptyDataException;
import itvdn.todolist.domain.PlainObjects.UserPojo;
import itvdn.todolist.domain.User;
import itvdn.todolist.repositories.UserRepository;
import itvdn.todolist.services.interfaces.IUserService;
import itvdn.todolist.utils.Convertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {

    private final Convertor convertor;
    private final UserRepository userRepository;

    @Autowired
    public UserService(Convertor convertor, UserRepository userRepository) {
        this.convertor = convertor;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserPojo createUser(User user) {
        userRepository.save(user);
        return convertor.userToPojo(user);
    }

    @Override
    @Transactional
    public UserPojo findUserByEmailAndPassword(String email, String password) {
        Optional<User> userOptional = userRepository.findByEmailAndPassword(email, password);
        if (userOptional.isPresent()) {
            return convertor.userToPojo(userOptional.get());
        } else {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public UserPojo getUser(long id) {
        Optional<User> foundUserOptional = userRepository.findById(id);

        if (foundUserOptional.isPresent()) {
            return convertor.userToPojo(foundUserOptional.get());
        } else {
            throw new CustomEmptyDataException("unable to get user");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserPojo> getAllUsers() {
        List<User> listOfUsers = userRepository.findAll();
        if (!listOfUsers.isEmpty()) {
            return listOfUsers.stream().map(convertor::userToPojo).collect(Collectors.toList());
        } else {
            throw new EmptyResultDataAccessException("at least ", 1);
        }
    }

    @Override
    @Transactional
    public UserPojo updateUser(User source, long id) {
        Optional<User> userForUpdateOptional = userRepository.findById(id);
        if (userForUpdateOptional.isPresent()) {
            User target = userForUpdateOptional.get();
            target.setEmail(source.getEmail());
            target.setPassword(source.getPassword());
            userRepository.save(target);
            return convertor.userToPojo(target);
        } else {
            throw new CustomEmptyDataException("unable to update user");
        }
    }

    @Override
    @Transactional
    public String deleteUser(long id) {
        Optional<User> userForDeleteOptional = userRepository.findById(id);

        if (userForDeleteOptional.isPresent()) {
            userRepository.delete(userForDeleteOptional.get());
            return "User with id:" + id + " was successfully removed";
        } else {
            throw new CustomEmptyDataException("unable to delete user");
        }
    }
}
