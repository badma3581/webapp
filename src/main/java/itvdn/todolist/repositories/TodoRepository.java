package itvdn.todolist.repositories;

import itvdn.todolist.domain.Todo;
import itvdn.todolist.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TodoRepository extends CrudRepository<Todo, Long > {
    List<Todo> findAllByUser(User user);
}
